﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialCodeFirst
{
    class Compras
    {
        public Int32 compraId { get; set; }
        public DateTime dataCompra { get; set; }
        public Decimal valor { get; set; }
        public Int32 clienteId { get; set; }
    }
}
