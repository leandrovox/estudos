﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialCodeFirst
{
    class Cliente
    {
        public Int32 clienteId { get; set; }
        public String clienteNome { get; set; }

        //ligaçao com a classe de compras
        public virtual List<Compras> compras { get; set; }
    }
}
