import React from 'react'
import { useRecoilState } from 'recoil'
import { textState } from '../atoms/text'
import { charCountState } from '../atoms/charCount'

function TextInput() {
    const [text, setText] = useRecoilState(textState)

    const onChange = event => {
        setText(event.target.value)
    }

    return (
        <div>
            <input type="text" value={text} onChange={onChange} />
            <br />
            Echo: {text}
        </div>
    )

}

function CharacterCount() {
    const count  = useRecoilState(charCountState)

    return <> Character Count: {count} </>
}


export function CharacterCounter() {
    return (
        <div>
            <TextInput />
            <CharacterCount />
        </div>
    )
}