import React  from 'react'
import './Card.css'

function Card(props){
    return (
        <div className="expression-box" onClick={props.handleClick} data-js={props.text}>
            <img src={props.img} alt={props.text} data-js={props.text}/>
            <p className="info" data-js={props.text}>{props.text}</p>
        </div>
    )
}

export default Card