import React from 'react'
import './Header.css'




export default function Header(props) {    
    return (
        
        <div>
            <h1>Leitor de textos, com voz, usando REACTJS</h1>
            <h3>Escolha o tipo de voz</h3>
            <select onChange={props.handleClick}>
                {props.voices.map(voice => {
                    return <option value={voice.name}
                        textContent={`${voice.name} | ${voice.lang}`}>
                        {voice.name} </option>
                })}
            </select>
            <button className="btn btn-toggle">Inserir Texto</button>
        </div>
    )
}



