import React from 'react';
import './App.css';
import Card from './components/Card';
import Header from './components/Header';

class App extends React.Component{
  utterance = new SpeechSynthesisUtterance()
  constructor(props){
    super(props)
    this.state = {
      voices:[],
      voiceSelected: null
    }
  }

  loadVoices = () => {        
    const voices = speechSynthesis.getVoices()    
    this.setState({voices: voices})
  }

  componentDidMount() {
    speechSynthesis.onvoiceschanged = this.loadVoices
  }

  selectVoice(voice){
    const voices = this.state.voices
    const options  = voice.currentTarget
    for(let i = 0; i < voice.currentTarget.length; i++){
      if(options[i].selected){        
        const voiceSelected = voices.find(voice => voice.name === options[i].value)
        this.setState({voiceSelected: voiceSelected})
        break;
      }
    }
  }

  removeClassListActive(element){  
    const data = element.target.dataset.js  
    setTimeout(() => {
      const elementActive = document.querySelector(`[data-js="${data}"]`)
      elementActive.classList.remove('active')
    }, 1500);    
  }
  
  speak(element){    
    element.currentTarget.classList.add('active') 

    this.removeClassListActive(element)    
    this.utterance.text = element.target.dataset.js
    this.utterance.voice = this.state.voiceSelected
    speechSynthesis.speak(this.utterance)
  } 

  render() {
    
    const humanExpressions = [
      { img: "/assets/img/drink.jpg", text: "Estou com sede" },
      { img: "/assets/img/food.jpg", text: "Estou com fome" },
      { img: "/assets/img/tired.jpg", text: "Estou cansado" },
      { img: "/assets/img/hurt.jpg", text: "Estou machucado" },
      { img: "/assets/img/happy.jpg", text: "Estou feliz" },
      { img: "/assets/img/angry.jpg", text: "Estou com raiva" },
      { img: "/assets/img/sad.jpg", text: "Estou triste" },
      { img: "/assets/img/scared.jpg", text: "Estou assustado" },
      { img: "/assets/img/outside.jpg", text: "Quero ir lá fora" },
      { img: "/assets/img/home.jpg", text: "Quero ir pra casa" },
      { img: "/assets/img/school.jpg", text: "Quero ir para a escola" },
      { img: "/assets/img/grandma.jpg", text: "Quero ver a vovó" },
    ];

    

    return (
      <div>
       <Header voices={this.state.voices} handleClick={this.selectVoice.bind(this)} key="0"/>
       <div className="main">
         {
           humanExpressions.map(humanExpression => {
             const urlImg = process.env.PUBLIC_URL + humanExpression.img
             return <Card text={humanExpression.text}
               img={urlImg}
               key={humanExpression.text} 
               handleClick={this.speak.bind(this)}/>
           })
         }
       </div>
     </div>
    )
  }
}

export default App;
