const main = document.querySelector('main')
const buttonInsertText = document.querySelector('.btn-toggle')
const buttonReadText = document.querySelector('#read')
const textArea = document.querySelector('textarea')
const divTextBox = document.querySelector('.text-box')
const divCloseTextbox = document.querySelector('.close')
const selectVoice = document.querySelector('select')

const humanExpressions = [
  { img: "./img/drink.jpg", text: "Estou com sede" },
  { img: "./img/food.jpg", text: "Estou com fome" },
  { img: "./img/tired.jpg", text: "Estou cansado" },
  { img: "./img/hurt.jpg", text: "Estou machucado" },
  { img: "./img/happy.jpg", text: "Estou feliz" },
  { img: "./img/angry.jpg", text: "Estou com raiva" },
  { img: "./img/sad.jpg", text: "Estou triste" },
  { img: "./img/scared.jpg", text: "Estou assustado" },
  { img: "./img/outside.jpg", text: "Quero ir lá fora" },
  { img: "./img/home.jpg", text: "Quero ir pra casa" },
  { img: "./img/school.jpg", text: "Quero ir para a escola" },
  { img: "./img/grandma.jpg", text: "Quero ver a vovó" },
];

const utterance = new SpeechSynthesisUtterance()

//funções para selecionar voz e reproduzir o texto
const setTextMessage = text => utterance.text = text
const speakText = () => speechSynthesis.speak(utterance)
const setSelectVoice = event => {
  utterance.voice = voices.find(voice => voice.name === event.target.value)
}

//adicionando as divs no DOM
const addExpressionBoxesIntoDOM = () => {
  const divs = humanExpressions.map(({ img, text }) =>
    ` <div class="expression-box" data-js="${text}">
          <img src="${img}" alt="${text}" data-js="${text}">
          <p class="info" data-js="${text}">${text}</p>
      </div>`
  ).join('')

  main.innerHTML = divs
}

addExpressionBoxesIntoDOM()

//estilo ao clicar
const setStyleClickedEvent = dataValue => {
  const div = document.querySelector(`[data-js="${dataValue}"]`)
  div.classList.add('active')

  setTimeout(() => {
    div.classList.remove('active')
  }, 1000);
}

//adicionar click da div
main.addEventListener('click', event => {
  const clickedElement = event.target
  const clickedElementMustBeSpeak = ['img', 'p'].some(element =>
    clickedElement.tagName.toLowerCase() === element.toLocaleLowerCase())

  if (clickedElementMustBeSpeak) {
    const clickedElementText = clickedElement.dataset.js
    setTextMessage(clickedElementText)
    speakText()
    setStyleClickedEvent(clickedElementText)
  }
})

let voices = speechSynthesis.getVoices()

speechSynthesis.addEventListener('voiceschanged', () => {
  voices = speechSynthesis.getVoices()
  updateSelectVoides(voices)
})

const clearSelect = () => {
  for (let i = 0; i < selectVoice.options.length; i++)
    selectVoice.remove(i)
}

const createOption = (accumulator, voice) => {
  accumulator += `<option 
          value="${voice.name}" 
          textContent="${voice.lang} | ${voice.name}">${voice.name}</option>`
  return accumulator
}


const updateSelectVoides = voices => {
  clearSelect()
  const optionElements = voices.reduce(createOption, '')
  selectVoice.innerHTML = optionElements  
}

updateSelectVoides(voices)

buttonInsertText.addEventListener('click', () => {
  divTextBox.classList.add('show')
})

buttonReadText.addEventListener('click', () => {
  setTextMessage(textArea.value)
  speakText()
})

divCloseTextbox.addEventListener('click', () => {
  divTextBox.classList.remove('show')
})

selectVoice.addEventListener('change', setSelectVoice)