angular
    .module('app')
    .factory('alunosFactory', AlunosFactory);

//function AlunosFactory(){}

function AlunosFactory($q, $http){
    return {
        listar: function(){
            var promessa = $q.defer();

            $http.get("http://localhost:51387/api/Aluno")
                .then(function(result){
                    console.log(result);
                    var alunos = [];

                    angular.forEach(result.data, function(aluno, id){
                        aluno.id = id;
                        alunos.push(aluno);
                    });

                    promessa.resolve(alunos);
                })

            return promessa.promise;
        }
    }
}/**/