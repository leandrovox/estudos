angular.module('app').controller("AlunoController", AlunoController);

function AlunoController($scope, alunosFactory){
    alunosFactory.listar().then(function(alunos){
        $scope.alunos = alunos;
    }) 
    
    /*[{
        nome: "Leandro",
        sobrenome: "Freitas",
        idade: 28,
        cpf: '3777878488',
        curso: "C"
    },{
        nome: "Leandro",
        sobrenome: "Freitas",
        idade: 28,
        cpf: '3777878488',
        curso: "C"
    }]*/

    $scope.adicionar = function(){
        $scope.alunos.push ({
            nome: $scope.nome,
            sobrenome: $scope.sobrenome,
            idade: $scope.idade,
            cpf: $scope.cpf,
            curso: $scope.curso
        })

        $scope.nome = '';
        $scope.sobrenome = '';
        $scope.idade = '';
        $scope.cpf = '';
        c$scope.curso = '';
    }
}