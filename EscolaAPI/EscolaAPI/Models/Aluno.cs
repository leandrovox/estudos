﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EscolaAPI.Models
{
    public class Aluno
    {
        public long Id { get; set; }
        public String nome { get; set; }
        public String sobrenome { get; set; }
        public long idade { get; set; }
        public String cpf { get; set; }
        public String curso { get; set; }

    }
}
