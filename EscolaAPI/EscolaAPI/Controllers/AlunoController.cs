﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscolaAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace EscolaAPI.Controllers
{
    [Route("api/[controller]")] 
    public class AlunoController : Controller
    {

        private readonly AlunoContext _context;

        public AlunoController(AlunoContext context)
        {
            this._context = context;

            if(this._context.AlunoItems.Count() == 0)
            {
                this._context.AlunoItems.Add(new Aluno { nome = "Leandro", sobrenome = "Freitas", idade = 28, cpf = "33535848799", curso = "C" });
                this._context.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<Aluno> GetALL()
        {
            return this._context.AlunoItems.ToList();
        }

        [HttpGet("{id}", Name = "GetAluno")]
        public IActionResult GetById(long id)
        {
            var item = this._context.AlunoItems.FirstOrDefault(t => t.Id == id);
            if(item == null)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }
        [HttpPost]
        public IActionResult Create([FromBody] Aluno aluno)
        {
            if(aluno == null)
            {
                return BadRequest();
            }

            this._context.AlunoItems.Add(aluno);
            this._context.SaveChanges();

            return CreatedAtRoute("GetAluno", new { id = aluno.Id }, aluno);
        }
    }
}