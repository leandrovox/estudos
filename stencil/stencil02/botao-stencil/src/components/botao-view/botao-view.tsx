import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'botao-view',
  styleUrl: 'botao-view.css',
  shadow: true
})
export class BotaoView {

  @Prop() apiKey: string;

  componentDidLoad(){
    console.log(this.apiKey)
  }

  render() {
    return (<Host>
      <slot></slot>
    </Host>
    );
  }

}
