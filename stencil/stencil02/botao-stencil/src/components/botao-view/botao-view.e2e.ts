import { newE2EPage } from '@stencil/core/testing';

describe('botao-botao-view', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<botao-botao-view></botao-botao-view>');

    const element = await page.find('botao-botao-view');
    expect(element).toHaveClass('hydrated');
  });
});
