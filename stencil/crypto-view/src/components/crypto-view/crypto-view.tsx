import { Component, Host, h, Prop, State } from '@stencil/core';
import { getCryptoDate } from '../../utils/utils';
import { CryptoInterface } from './crypto-interface';

@Component({
  tag: 'crypto-view',
  styleUrl: 'crypto-view.css',
  shadow: true
})
export class View {

  @Prop() apiKey: string
  @State() cryptoData: CryptoInterface
  cryptoCurrencies = ['BTC', 'ETH', 'XRP'];

  componentDidLoad(){
      getCryptoDate(this.apiKey).then(data =>{
        console.log(data)
        this.cryptoData = data
        })
  }

  render() {
    return (
      <Host>
        <b>Crypto data on date: {new Date().toLocaleDateString()}</b>
        <crypto-table cryptoData={this.cryptoData} cryptoCurrencies={this.cryptoCurrencies}></crypto-table>
        <slot></slot>
      </Host>
    );
  }

}
