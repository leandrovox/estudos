import { Component, Host, h, Prop } from '@stencil/core';
import { CryptoInterface } from '../crypto-view/crypto-interface';

@Component({
  tag: 'crypto-table',
  styleUrl: 'crypto-table.css',
  shadow: false,
  scoped: true
})
export class Table {

  @Prop() cryptoData: CryptoInterface
  @Prop() cryptoCurrencies: string[]

  render() {
    return (
      <Host>
        {/* <table class={'crypto'}>
          <tr>
            <td></td>
            <td>USD</td>
            <td>EUR</td>
          </tr>
          {this.cryptoCurrencies.map((item) => {
            return this.cryptoData && item in this.cryptoData ? <tr>
              <td>{item}</td>
              <td>{this.cryptoData[item].USD}</td>
              <td>{this.cryptoData[item].EUR}</td>
            </tr> : null
          })
          }
        </table> */}
      </Host>
    );
  }

}
