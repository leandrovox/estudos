import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Grid from './src/components/Grid';
import LineGrid from './src/components/LineGrid';
import {ScreenOrientation} from 'expo'

export default class App extends Component{
  state = {
    data: [
      { id: "00", name: "Relâmpago McQueen" },
      { id: "01", name: "Agente Tom Mate" },
      { id: "02", name: "Doc Hudson" },
      { id: "03", name: "Cruz Ramirez" },
      { id: "04", name: "Relâmpago McQueen" },
      { id: "05", name: "Agente Tom Mate" },
      { id: "06", name: "Doc Hudson" },
      { id: "07", name: "Cruz Ramirez" }
    ]
  }

  componentDidMount(){
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.LANDSCAPE)
  }

  render(){
    return (
      <View style={this.styles.container}>
        {/* <Grid data={this.state.data}/>  */}
        <LineGrid />
      </View>
    );
  }
  


  styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fffccc',
      marginTop: 50
    },
  });
}