import React from 'react'
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native'


export default props => 
    <View style={styles.container}>
        <Text style={styles.item_name}>Raquete</Text>
        <Text style={styles.item_value}>R$1.50</Text>
        <TouchableOpacity>
            <Text style={styles.button}>-</Text>
        </TouchableOpacity>
        <Text style={styles.item_value}>10</Text>
        <TouchableOpacity>
            <Text style={styles.button}>+</Text>
        </TouchableOpacity> 
        <Text style={styles.item_value}>30%</Text>
        <Text style={styles.item_value}>R$10.50</Text>
        <Text style={styles.item_value}>1.75%</Text>
    </View>

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        margin: 5
    },
    item_name:{
        fontSize:20,
        fontWeight:"bold"
    },
    item_value:{
        fontSize: 20,
        alignItems: "flex-end"
    },
    button:{
        backgroundColor:"#fcc"
    }
})