import React from 'react'
import {SafeAreaView, View, Text, StyleSheet, FlatList} from 'react-native'



export default props =>
    <SafeAreaView>        
        <FlatList 
            data= {props.data}
            keyExtractor={item => item.id}
            numColumns={3}
            renderItem={({item}) => {
                return(
                    <View style={styles.item}>
                        <Text style={styles.text}> {item.name} </Text>
                    </View>
                )
            }
        }
        />
    </SafeAreaView>

const styles = StyleSheet.create({
    item: {
      alignItems: "center",
      backgroundColor: "#dcda48",
      flexBasis: 0,
      flexGrow: 1,
      margin: 4,
      padding: 20
    },
    text: {
      color: "#333333"
    }
  });